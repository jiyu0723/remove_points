#include "removeOutliers.h"

void removeOutliers(CMeshO &cm)
{
	std::map<CMeshO::VertexPointer, CMeshO::VertexPointer> mp;

	CMeshO::VertexIterator  vi;
	

	std::vector<float> x_arr, y_arr, z_arr;

	size_t num_vert = cm.vert.size();

	std::vector<CMeshO::VertexPointer> perm(num_vert);
	
	int k = 0;
	for (vi = cm.vert.begin(); vi != cm.vert.end(); ++vi)
	{
		float x = (*vi).P()[0];
		float y = (*vi).P()[1];
		float z = (*vi).P()[2];

		perm[k] = &(*vi);
		k++;

		//std::cout << "x: " << x << " y: " << y << " z: " << z << std::endl;

		x_arr.push_back(x);
		y_arr.push_back(y);
		z_arr.push_back(z);

	}

	int vert_num = x_arr.size();

	std::sort(x_arr.begin(), x_arr.end());
	std::sort(y_arr.begin(), y_arr.end());
	std::sort(z_arr.begin(), z_arr.end());


	float x_cen = x_arr[vert_num / 2];
	float y_cen = y_arr[vert_num / 2];
	float z_cen = z_arr[vert_num / 2];
	std::cout << "center x: " << x_cen << " y: " << y_cen << " z: " << z_cen << std::endl;

	int i = 0;
	int delete_count = 0;
	for (vi = cm.vert.begin(); vi != cm.vert.end(); ++vi)
	{
		float x_dist = abs((*vi).P()[0] - x_cen);
		float y_dist = abs((*vi).P()[1] - y_cen);
		float z_dist = abs((*vi).P()[2] - z_cen);

		if (x_dist > 1 || y_dist > 1 || z_dist > 1.5)
		{
			//std::cout << "x: " << (*vi).P()[0] << " y: " << (*vi).P()[1] << " z: " << (*vi).P()[2] << std::endl;

			CMeshO::VertexPointer vp = &(*vi);
			Allocator<CMeshO>::DeleteVertex(cm, *vp);

			delete_count++;
		}

	}
	vcg::tri::Allocator<CMeshO>::CompactEveryVector(cm);
	std::cout << "before delete outliers, we have: " << num_vert << " vertices" << std::endl;
	num_vert = cm.vert.size();
	std::cout << "deleted: " << delete_count << " vertices" << std::endl;
	std::cout << "after delete outliers, now we have: " << num_vert << " vertices" << std::endl;
}