// Remove_Outlier_Points.cpp : Defines the entry point for the console application.
//

#include "removeOutliers.h"


int main(int argc, char*argv[])
{
	std::string input_file = argv[1];
	std::string output_file = argv[2];

	CMeshO mesh;

	mesh.vert.EnableColor();
	mesh.vert.EnableNormal();

	int status = vcg::tri::io::ImporterPLY<CMeshO>::Open(mesh, input_file.c_str());
	if (status != 0) // all the importers return 0 on success
	{
		if (vcg::tri::io::ImporterPLY<CMeshO>::ErrorCritical(status))
		{
			if (vcg::tri::io::ImporterPLY<CMeshO>::ErrorCritical(status)) {
				std::cerr << "Load file error:  , " << vcg::tri::io::ImporterPLY<CMeshO>::ErrorMsg(status);//<< flush;
				exit(0);
			}
		}
	}

	removeOutliers(mesh);

	// write out ply .........................................................................................
	vcg::tri::io::ExporterPLY<CMeshO>::Save(mesh, output_file.c_str(), vcg::tri::io::Mask::IOM_VERTNORMAL | vcg::tri::io::Mask::IOM_VERTCOLOR, 1);


    return 0;
}

