#pragma once

#include "wrap\io_trimesh\import_ply.h"
#include "wrap\io_trimesh\export_ply.h"


#include "mesh.h"

#include <iostream>     // std::cout
#include <algorithm>    // std::sort
#include <vector>       // std::vector


void removeOutliers(CMeshO &mesh_input);